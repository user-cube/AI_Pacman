# Artificial Inteligence - Pacman
Client-Server PACMAN clone

# Install

* Clone this repository
* Create a virtual environment:

```console
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```
* Intall xterm:

Ubuntu based:
```
$ sudo apt update
$ sudo apt install xterm
```
Fedora:
```
$ sudo dnf update
$ sudo dnf install xterm
```

Arch based:
```
$ yaourt xterm
```

# How to run:
Run: 
```
source venv/bin/actibate
```
After that execute:
```
$ ./run.sh
```

## Multiple executions
Open 2 terminals and run:

Terminal 1:
```
$ ./deployServer.sh
```
Terminal 2:
```
$ python3 multiple.py
```


# Credits
Sprites from https://github.com/rm-hull/big-bang/tree/master/examples/pacman/data
